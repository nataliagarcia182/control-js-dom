'use strict';

// puntuaciones

let puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

let resultado = [];
puntuaciones.forEach((a) => {
  resultado.push({
    equipo: a.equipo,
    puntos: a.puntos.reduce((a, b) => a + b, 0),
  });
});

resultado.sort(function (a, b) {
  return a.puntos - b.puntos;
});
console.log('Equipo con menos puntos', resultado[0]);

let masPuntos = resultado.sort(function (a, b) {
  return b.puntos - a.puntos;
});
console.log('Equipo con más puntos', resultado[0]);
