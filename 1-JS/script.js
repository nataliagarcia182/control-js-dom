"use strict";
let min = 0;
let max = 100;
let stop = 0;
let contador = 0;

let numeroUsuario;
const numeroAleatorio = Math.floor(Math.random() * (max - min)) + min;

function ponerNumeros() {
  while (stop < 6) {
    numeroUsuario = parseInt(prompt("Escribe un número..."));

    stop++;
    contador += stop;
    if (stop === 5) {
      alert("Ohh perdiste");
      break;
    }
    if (numeroUsuario > numeroAleatorio) {
      alert("Pon un número menor");
    }
    if (numeroUsuario < numeroAleatorio) {
      alert("Pon un número mayor");
    }

    if (numeroUsuario === numeroAleatorio) {
      alert("¡BIEN! Has ganado");
      break;
    }
  }
}
console.log(numeroAleatorio);
ponerNumeros();
