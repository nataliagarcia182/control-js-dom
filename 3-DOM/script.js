'use strict';

let html = document.querySelector('body');

const div = document.createElement('div');

html.appendChild(div);

setInterval(function () {
  let reloj = new Date();
  let horas = reloj.getHours();
  let minutos = reloj.getMinutes();
  let segundos = reloj.getSeconds();

  horas < 10 ? (horas = '0' + horas) : horas;

  minutos < 10 ? (minutos = '0' + minutos) : minutos;

  segundos < 10 ? (segundos = '0' + segundos) : segundos;

  div.innerHTML = horas + ':' + minutos + ':' + segundos;
}, 1000);
